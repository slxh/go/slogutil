package syslog

import (
	"context"
	"io"
	"log/slog"
)

// Handler implements a [slog.Handler] that logs to syslog.
// It embeds a [slog.Handler] to perform the formatting of messages,
// and a [Writer] that the [slog.Handler] writes to.
type Handler struct {
	Handler slog.Handler
	Writer  *Writer
}

// NewTextHandler creates a [Handler] that logs messages to the syslog using [slog.TextHandler].
// If opts is `nil`, [DefaultWriterOptions] is used. See [slog.NewTextHandler] for details on the TextHandler.
func NewTextHandler(opts *WriterOptions, slogOpts *slog.HandlerOptions) (*Handler, error) {
	return NewHandler(opts, func(writer io.Writer) slog.Handler {
		return slog.NewTextHandler(writer, slogOpts)
	})
}

// NewJSONHandler creates a [Handler] that logs messages to the syslog using [slog.JSONHandler].
// If opts is `nil`, [DefaultWriterOptions] is used. See [slog.NewJSONHandler] for details on the JSONHandler.
func NewJSONHandler(opts *WriterOptions, slogOpts *slog.HandlerOptions) (*Handler, error) {
	return NewHandler(opts, func(writer io.Writer) slog.Handler {
		return slog.NewJSONHandler(writer, slogOpts)
	})
}

// NewHandler initializes a [Handler] based on another handler.
// The other handler must be created by the handlerFunc parameter, which is called immediately.
func NewHandler(opts *WriterOptions, handlerFunc func(io.Writer) slog.Handler) (*Handler, error) {
	w, err := NewWriter(opts)
	if err != nil {
		return nil, err
	}

	return &Handler{Writer: w, Handler: handlerFunc(w)}, nil
}

// Enabled reports whether the handler handles records at the given level.
func (s *Handler) Enabled(ctx context.Context, level slog.Level) bool {
	return s.Handler.Enabled(ctx, level)
}

// Handle calls the Handle function of the underlying [slog.Handler]
// with the log level communicated to the embedded [Writer].
func (s *Handler) Handle(ctx context.Context, record slog.Record) (err error) {
	s.Writer.WithLevel(record.Level, func() {
		err = s.Handler.Handle(ctx, record)
	})

	return
}

// WithAttrs returns a new [slog.Handler] with the given attributes set.
func (s *Handler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return &Handler{Handler: s.Handler.WithAttrs(attrs), Writer: s.Writer}
}

// WithGroup returns a new [slog.Handler] with the given group appended to the existing groups.
func (s *Handler) WithGroup(name string) slog.Handler {
	return &Handler{Handler: s.Handler.WithGroup(name), Writer: s.Writer}
}
