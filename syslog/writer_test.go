package syslog_test

import (
	"log/slog"
	logsyslog "log/syslog"
	"net"
	"testing"

	"gitlab.com/slxh/go/slogutil/syslog"
)

func newWriter(t *testing.T) (*testServer, *syslog.Writer) {
	t.Helper()

	ts := newTestServer(t)

	w, err := syslog.NewWriter(&syslog.WriterOptions{
		Tag:      "go_test",
		Network:  "tcp",
		Address:  ts.ListenAddr(),
		Facility: syslog.FacilityLocal0,
		Level:    syslog.LevelNotice,
	})
	if err != nil {
		t.Fatal(err)
	}

	return ts, w
}

func TestNewWriter(t *testing.T) {
	_, w := newWriter(t)

	assertErrorIs(t, nil, w.Close())
}

func TestNewWriter_Default(t *testing.T) {
	if _, err := logsyslog.New(0, ""); err != nil {
		t.SkipNow()
	}

	w, err1 := syslog.NewWriter(nil)
	err2 := w.Close()

	assertErrorIs(t, nil, err1)
	assertErrorIs(t, nil, err2)
	assertErrorIs(t, nil, w.Close())
}

func TestNewWriter_Default_Error(t *testing.T) {
	if _, err := logsyslog.New(0, ""); err == nil {
		t.SkipNow()
	}

	w, err1 := syslog.NewWriter(nil)

	assertEqual(t, nil, w)
	assertNotNil(t, err1)
}

func TestWriter_WithLevel(t *testing.T) {
	ts, w := newWriter(t)
	exp := "<128>0000-00-00T00:00:00+00:00 hostname go_test[0000]: test"

	w.WithLevel(syslog.LevelEmerg, func() {
		n, err := w.Write([]byte("test"))

		assertEqual(t, 4, n)
		assertErrorIs(t, nil, err)
	})

	assertEqual(t, exp, ts.String())
	assertErrorIs(t, nil, w.Close())
}

func TestWriter_Write(t *testing.T) {
	ts, w := newWriter(t)
	exp := "<134>0000-00-00T00:00:00+00:00 hostname go_test[0000]: test write"
	n, err := w.Write([]byte("test write"))

	assertErrorIs(t, nil, err)
	assertEqual(t, 10, n)
	assertEqual(t, exp, ts.String())
	assertErrorIs(t, nil, w.Close())
}

func TestWriter_Write_Error(t *testing.T) {
	ts, w := newWriter(t)
	exp := new(net.OpError)
	err1 := ts.Close()
	err2 := w.Close()

	n, err3 := w.Write([]byte("test write"))

	assertErrorIs(t, nil, err1)
	assertErrorIs(t, nil, err2)
	assertErrorAs(t, &exp, err3)
	assertEqual(t, 0, n)
	assertEqual(t, "", ts.String())
	assertErrorIs(t, nil, w.Close())
}

func TestWriter_WriteLevel(t *testing.T) {
	tests := map[slog.Level]string{
		syslog.LevelEmerg:   "<128>0000-00-00T00:00:00+00:00 hostname go_test[0000]: test",
		syslog.LevelAlert:   "<129>0000-00-00T00:00:00+00:00 hostname go_test[0000]: test",
		syslog.LevelCrit:    "<130>0000-00-00T00:00:00+00:00 hostname go_test[0000]: test",
		syslog.LevelErr:     "<131>0000-00-00T00:00:00+00:00 hostname go_test[0000]: test",
		syslog.LevelWarning: "<132>0000-00-00T00:00:00+00:00 hostname go_test[0000]: test",
		syslog.LevelNotice:  "<133>0000-00-00T00:00:00+00:00 hostname go_test[0000]: test",
		syslog.LevelInfo:    "<134>0000-00-00T00:00:00+00:00 hostname go_test[0000]: test",
		syslog.LevelDebug:   "<135>0000-00-00T00:00:00+00:00 hostname go_test[0000]: test",
	}

	for level, exp := range tests {
		t.Run(level.String(), func(t *testing.T) {
			ts, w := newWriter(t)

			assertErrorIs(t, nil, w.WriteLevel(level, []byte("test")))
			assertEqual(t, exp, ts.String())
			assertErrorIs(t, nil, w.Close())
		})
	}
}
