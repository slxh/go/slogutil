// Package syslog contains functionality for using [slog] with Unix syslog servers,
// using the [syslog] package.
// It provides a [Handler] that wraps another [slog.Handler].
// The [Handler] will ensure that the embedded [slog.Handler] writes to the
// syslog at the correct log level.
//
// The [NewJSONHandler] and [NewTextHandler] functions can be used to setup
// logging using [slog.JSONHandler] or [slog.TextHandler] respectively.
// These can be used as follows:
//
//	package main
//
//	import (
//		"log/slog"
//
//		"gitlab.com/slxh/go/slogutil/syslog"
//	)
//
//	func main() {
//		h, err := syslog.NewTextHandler(nil, nil)
//		if err != nil {
//			panic(err)
//		}
//
//		slog.SetDefault(slog.New(h))
//		slog.Info("Hello!") // written to syslog
//	}
//
// This will be shown in your syslog as something similar to the following:
//
//	aug 09 14:16:21 hostname example[542251]: time=2023-08-09T14:16:21.173+02:00 level=INFO msg=Hello!
//
// Use [NewHandler] to use a custom handler for formatting, for example:
//
//	h, err := syslog.NewHandler(nil, func(writer io.Writer) slog.Handler {
//		return &MyCustomHandler{}
//	})
package syslog
