package syslog_test

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"regexp"
	"strings"
	"sync"
	"testing"
	"time"
)

type lockedBuffer struct {
	buf bytes.Buffer
	mu  sync.Mutex
}

func (l *lockedBuffer) Read(p []byte) (n int, err error) {
	l.mu.Lock()
	defer l.mu.Unlock()

	return l.buf.Read(p)
}

func (l *lockedBuffer) Write(p []byte) (n int, err error) {
	l.mu.Lock()
	defer l.mu.Unlock()

	return l.buf.Write(p)
}

func (l *lockedBuffer) String() string {
	l.mu.Lock()
	defer l.mu.Unlock()

	return l.buf.String()
}

type testServer struct {
	net.Listener
	Buffer *lockedBuffer
}

func newTestServer(t *testing.T) *testServer {
	t.Helper()

	l, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		t.Fatal(err)
	}

	s := &testServer{Listener: l, Buffer: new(lockedBuffer)}

	go func() {
		conn, err := l.Accept()
		if err != nil {
			return
		}

		_, _ = io.Copy(s.Buffer, conn)
	}()

	return s
}

func (ts *testServer) String() string {
	time.Sleep(10 * time.Millisecond)

	return strings.TrimSuffix(replaceVariable(ts.Buffer.String()), "\n")
}

func (ts *testServer) ListenAddr() string {
	return ts.Listener.Addr().String()
}

func assertErrorIs(t *testing.T, exp, got error) { //nolint:unparam // keep for future use
	t.Helper()

	if !errors.Is(got, exp) {
		t.Errorf("Incorrect error: expected %#v, got %#v", exp, got)
	}
}

func assertErrorAs(t *testing.T, exp any, got error) {
	t.Helper()

	if !errors.As(got, exp) {
		t.Errorf("Incorrect error: expected %#v, got %#v", exp, got)
	}
}

func assertEqual[T comparable](t *testing.T, exp, got T) {
	t.Helper()

	if exp != got {
		t.Errorf("Not equal: \n    expected: %q\n         got: %q",
			fmt.Sprintf("%v", exp), fmt.Sprintf("%v", got))
	}
}

func assertNotNil(t *testing.T, got any) {
	t.Helper()

	if got == nil {
		t.Error("Got nil")
	}
}

func replaceVariable(s string) string {
	hostname, _ := os.Hostname()

	s = strings.ReplaceAll(s, hostname, "hostname")
	s = regexp.MustCompile(`\[\d+\]`).ReplaceAllString(s, "[0000]")
	s = regexp.MustCompile(`\d{4}`).ReplaceAllString(s, "0000")
	s = regexp.MustCompile(`([:T.+-])\d{2,3}`).ReplaceAllString(s, "${1}00")
	s = regexp.MustCompile(`:00Z`).ReplaceAllString(s, ":00+00:00")
	s = regexp.MustCompile(`\.\d{2}\d+`).ReplaceAllString(s, ".00")

	return s
}
