package syslog

import (
	"fmt"
	"log/slog"
	"log/syslog"
	"sync"
)

// These [slog.Level] values can be used when logging to the syslog.
// It is generally advisable to limit usage to the levels defined in [slog],
// as this will be correctly annotated in log contents.
const (
	LevelEmerg   = slog.LevelError + 3
	LevelAlert   = slog.LevelError + 2
	LevelCrit    = slog.LevelError + 1
	LevelErr     = slog.LevelError
	LevelWarning = slog.LevelWarn
	LevelNotice  = slog.LevelInfo + 1
	LevelInfo    = slog.LevelInfo
	LevelDebug   = slog.LevelDebug
)

// Facilities that can be logged to.
// This describes the type of message that is logged.
// The facility may determine how the message is handled, and where it is stored.
const (
	FacilityKern     = syslog.LOG_KERN
	FacilityUser     = syslog.LOG_USER
	FacilityMail     = syslog.LOG_MAIL
	FacilityDaemon   = syslog.LOG_DAEMON
	FacilityAuth     = syslog.LOG_AUTH
	FacilitySyslog   = syslog.LOG_SYSLOG
	FacilityLPR      = syslog.LOG_LPR
	FacilityNews     = syslog.LOG_NEWS
	FacilityUUCP     = syslog.LOG_UUCP
	FacilityCron     = syslog.LOG_CRON
	FacilityAuthPriv = syslog.LOG_AUTHPRIV
	FacilityFTP      = syslog.LOG_FTP
	FacilityLocal0   = syslog.LOG_LOCAL0
	FacilityLocal1   = syslog.LOG_LOCAL1
	FacilityLocal2   = syslog.LOG_LOCAL2
	FacilityLocal3   = syslog.LOG_LOCAL3
	FacilityLocal4   = syslog.LOG_LOCAL4
	FacilityLocal5   = syslog.LOG_LOCAL5
	FacilityLocal6   = syslog.LOG_LOCAL6
	FacilityLocal7   = syslog.LOG_LOCAL7
)

// DefaultWriterOptions contains the default [WriterOptions] that are used when
// a nil parameter is passed to the [Writer] initialization.
var DefaultWriterOptions = &WriterOptions{ //nolint:gochecknoglobals // used as constant
	Facility: syslog.LOG_DAEMON,
}

// WriterOptions contains options for the syslog [Writer].
type WriterOptions struct {
	// Tag contains the syslog tag (log prefix) used by the Writer.
	// If left empty, os.Args[0] is used.
	Tag string

	// Network contains the network to use for connecting to a remote server.
	// If left empty, the Writer will connect to the local syslog server.
	Network string

	// Address contains the remote address of the remote server.
	Address string

	// Facility contains the facility of the Writer.
	// This defaults to FacilityKern.
	Facility syslog.Priority

	// Level contains the default level of the Writer.
	// This defaults to slog.LevelInfo.
	Level slog.Level
}

// Writer is an extended [syslog.Writer] with support for [slog.Level].
type Writer struct {
	*syslog.Writer

	defaultLevel slog.Level
	level        slog.Level
	mu           sync.Mutex
}

// NewWriter initializes and connects to the log server defined by the given options.
// If the given options are `nil`, [DefaultWriterOptions] are used.
func NewWriter(opts *WriterOptions) (*Writer, error) {
	if opts == nil {
		opts = DefaultWriterOptions
	}

	w, err := syslog.Dial(opts.Network, opts.Address, opts.Facility, opts.Tag)
	if err != nil {
		return nil, fmt.Errorf("unable to connect to syslog: %w", err)
	}

	return &Writer{Writer: w, defaultLevel: opts.Level}, nil
}

// WithLevel runs the given function while the default log level is set to the given value.
// The log level is restored to the default value afterward.
func (s *Writer) WithLevel(level slog.Level, fn func()) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.setLevel(level)
	defer s.setLevel(s.defaultLevel)

	fn()
}

func (s *Writer) setLevel(level slog.Level) {
	s.level = level
}

// Write data to the syslog using the configured log level.
// When called in [WithLevel], this function uses the level defined in that function.
func (s *Writer) Write(p []byte) (int, error) {
	if err := s.WriteLevel(s.level, p); err != nil {
		return 0, err
	}

	return len(p), nil
}

// WriteLevel writes a message to the syslog with the given level.
func (s *Writer) WriteLevel(level slog.Level, p []byte) error {
	if err := s.writeLevel(level, string(p)); err != nil {
		return fmt.Errorf("cannot write to log: %w", err)
	}

	return nil
}

//nolint:wrapcheck,revive // wrapped when called in WriteLevel.
func (s *Writer) writeLevel(level slog.Level, m string) error {
	switch {
	case level >= LevelEmerg:
		return s.Emerg(m)
	case level >= LevelAlert:
		return s.Alert(m)
	case level >= LevelCrit:
		return s.Crit(m)
	case level >= LevelErr:
		return s.Err(m)
	case level >= LevelWarning:
		return s.Warning(m)
	case level >= LevelNotice:
		return s.Notice(m)
	case level >= LevelInfo:
		return s.Info(m)
	default:
		return s.Debug(m)
	}
}
