package syslog_test

import (
	"context"
	"fmt"
	"io"
	"log/slog"
	"testing"
	"time"

	"gitlab.com/slxh/go/slogutil/syslog"
)

func TestNewTextHandler(t *testing.T) {
	ts := newTestServer(t)
	h, err := syslog.NewTextHandler(&syslog.WriterOptions{
		Tag:      "go_test",
		Network:  "tcp",
		Address:  ts.ListenAddr(),
		Facility: syslog.FacilityLocal0,
		Level:    syslog.LevelNotice,
	}, nil)

	assertErrorIs(t, nil, err)
	assertNotNil(t, h)
}

func TestNewJSONHandler(t *testing.T) {
	ts := newTestServer(t)
	h, err := syslog.NewJSONHandler(&syslog.WriterOptions{
		Tag:      "go_test",
		Network:  "tcp",
		Address:  ts.ListenAddr(),
		Facility: syslog.FacilityLocal0,
		Level:    syslog.LevelNotice,
	}, nil)

	assertErrorIs(t, nil, err)
	assertNotNil(t, h)
}

func TestNewHandler(t *testing.T) {
	opts := &syslog.WriterOptions{
		Network: "tcp",
		Address: "test.invalid:1234",
	}
	_, err := syslog.NewHandler(opts, func(writer io.Writer) slog.Handler {
		return &testHandler{w: writer}
	})

	assertNotNil(t, err)
}

type testHandler struct {
	w      io.Writer
	attrs  []slog.Attr
	groups []string
}

func (*testHandler) Enabled(_ context.Context, _ slog.Level) bool {
	return true
}

func (t *testHandler) Handle(_ context.Context, record slog.Record) error {
	_, err := t.w.Write([]byte(fmt.Sprintf("msg=%q attrs=%v groups=%v", record.Message, t.attrs, t.groups)))

	return err
}

func (t *testHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return &testHandler{w: t.w, attrs: append(t.attrs, attrs...), groups: t.groups}
}

func (t *testHandler) WithGroup(group string) slog.Handler {
	return &testHandler{w: t.w, attrs: t.attrs, groups: append(t.groups, group)}
}

func newTestHandler(t *testing.T) (*testServer, *syslog.Handler) {
	t.Helper()

	ts := newTestServer(t)
	opts := &syslog.WriterOptions{
		Tag:      "go_test",
		Network:  "tcp",
		Address:  ts.ListenAddr(),
		Facility: syslog.FacilityLocal0,
		Level:    syslog.LevelNotice,
	}
	fn := func(writer io.Writer) slog.Handler {
		return &testHandler{w: writer}
	}

	h, err := syslog.NewHandler(opts, fn)
	if err != nil {
		t.Fatal(err)
	}

	return ts, h
}

func TestHandler_Enabled(t *testing.T) {
	_, h := newTestHandler(t)

	assertEqual(t, true, h.Enabled(context.Background(), slog.LevelDebug))
}

func TestHandler_Handle(t *testing.T) {
	ts, h := newTestHandler(t)
	err := h.Handle(context.Background(), slog.NewRecord(time.Time{}, syslog.LevelCrit, "test", 0))
	exp := `<130>0000-00-00T00:00:00+00:00 hostname go_test[0000]: msg="test" attrs=[] groups=[]`

	assertEqual(t, exp, ts.String())
	assertErrorIs(t, nil, err)
}

func TestHandler_WithAddrs(t *testing.T) {
	ts, h := newTestHandler(t)

	err := h.WithAttrs([]slog.Attr{slog.Duration("duration", time.Millisecond)}).
		Handle(context.Background(), slog.NewRecord(time.Time{}, syslog.LevelCrit, "test", 0))
	exp := `<130>0000-00-00T00:00:00+00:00 hostname go_test[0000]: msg="test" attrs=[duration=1ms] groups=[]`

	assertEqual(t, exp, ts.String())
	assertErrorIs(t, nil, err)
}

func TestHandler_WithGroup(t *testing.T) {
	ts, h := newTestHandler(t)

	err := h.WithGroup("groupname").
		Handle(context.Background(), slog.NewRecord(time.Time{}, syslog.LevelCrit, "test", 0))
	exp := `<130>0000-00-00T00:00:00+00:00 hostname go_test[0000]: msg="test" attrs=[] groups=[groupname]`

	assertEqual(t, exp, ts.String())
	assertErrorIs(t, nil, err)
}
