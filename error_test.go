package slogutil_test

import (
	"fmt"
	"io/fs"
	"log/slog"
	"os"

	"gitlab.com/slxh/go/slogutil"
)

func ExampleError() {
	log := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		ReplaceAttr: slogutil.RemoveTime,
	}))

	err := slogutil.Error(fs.ErrNotExist, "path", "/foo/bar", "action", "open")

	log.Error("Could not open file", "err", err)

	// Output:
	// level=ERROR msg="Could not open file" err.msg="file does not exist" err.path=/foo/bar err.action=open
}

func ExampleError_wrap() {
	log := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		ReplaceAttr: slogutil.RemoveTime,
	}))

	err1 := slogutil.Error(fs.ErrNotExist, "path", "/foo/bar")
	err2 := slogutil.Error(err1, "action", "open")

	log.Error("Could not open file", "err", err2)

	// Output:
	// level=ERROR msg="Could not open file" err.msg="file does not exist" err.action=open err.path=/foo/bar
}

func ExampleErrorMsg() {
	log := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		ReplaceAttr: slogutil.RemoveTime,
	}))

	err := slogutil.ErrorMsg("error opening file", fs.ErrNotExist, "path", "/foo/bar", "action", "open")

	log.Error("Could not open file", "err", err)
	fmt.Println(err)

	// Output:
	// level=ERROR msg="Could not open file" err.msg="file does not exist" err.path=/foo/bar err.action=open
	// error opening file: file does not exist
}

func ExampleErrorAttr() {
	log := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		ReplaceAttr: slogutil.RemoveTime,
	}))

	err := fmt.Errorf("wrapped: %w",
		slogutil.Error(fs.ErrPermission, "path", "/foo/bar"))

	attr1 := slogutil.ErrorAttr("err1", fs.ErrNotExist)
	attr2 := slogutil.ErrorAttr("err2", err)

	log.Error("Two errors", attr1, attr2)

	// Output:
	// level=ERROR msg="Two errors" err1="file does not exist" err2.msg="wrapped: permission denied" err2.path=/foo/bar
}
