package slogctx_test

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"slices"
	"testing"
	"time"

	"gitlab.com/slxh/go/slogutil"
	"gitlab.com/slxh/go/slogutil/slogctx"
)

func ExampleAdd() {
	ctx := slogctx.Add(context.Background(), "one", 1)

	slogctx.Add(ctx, "two", 2)
	slogctx.Add(ctx, "three", 3)

	log := slog.New(slogctx.NewHandler(slog.NewTextHandler(os.Stdout,
		&slog.HandlerOptions{ReplaceAttr: slogutil.RemoveTime})))

	log.InfoContext(ctx, "Hello")
	// Output:
	// level=INFO msg=Hello one=1 two=2 three=3
}

func ExampleWith() {
	ctx := slogctx.With(context.Background(), "one", 1)

	a := slogctx.With(ctx, "two", 2)
	b := slogctx.With(ctx, "three", 3)

	log := slog.New(slogctx.NewHandler(slog.NewTextHandler(os.Stdout,
		&slog.HandlerOptions{ReplaceAttr: slogutil.RemoveTime})))

	log.InfoContext(a, "A")
	log.InfoContext(b, "B")
	// Output:
	// level=INFO msg=A one=1 two=2
	// level=INFO msg=B one=1 three=3
}

func ExampleFrom() {
	ctx := slogctx.With(context.Background(), "key", "value")

	fmt.Printf("Args: %v", slogctx.From(ctx).Args())
	// Output:
	// Args: [key value]
}

func ExampleNewContext() {
	ctx := slogctx.NewContext("one", 1).Store(context.Background())

	log := slog.New(slogctx.NewHandler(slog.NewTextHandler(os.Stdout,
		&slog.HandlerOptions{ReplaceAttr: slogutil.RemoveTime})))

	log.InfoContext(ctx, "Hello")
	// Output:
	// level=INFO msg=Hello one=1
}

func TestContext_AddToRecord(t *testing.T) {
	rec := slog.Record{
		Time:    time.Unix(1722542402, 0),
		Message: "Test",
	}

	(*slogctx.Context)(nil).AddToRecord(&rec)

	if rec.NumAttrs() != 0 {
		t.Errorf("unexpected number of attrs: %d", rec.NumAttrs())
	}

	slogctx.NewContext("one", 1).AddToRecord(&rec)

	if rec.NumAttrs() != 1 {
		t.Errorf("unexpected number of attrs: %d", rec.NumAttrs())
	}
}

func ExampleContext_Add() {
	ctx := slogctx.Add(context.Background(), "one", 1)

	slogctx.From(ctx).Add("two", 2)
	slogctx.From(ctx).Add("three", 3)

	log := slog.New(slogctx.NewHandler(slog.NewTextHandler(os.Stdout,
		&slog.HandlerOptions{ReplaceAttr: slogutil.RemoveTime})))

	log.InfoContext(ctx, "Hello")
	// Output:
	// level=INFO msg=Hello one=1 two=2 three=3
}

func TestContext_Args(t *testing.T) {
	if (*slogctx.Context)(nil).Args() != nil {
		t.Errorf("unexpected args")
	}

	c := slogctx.NewContext(1, 2)

	if !slices.Equal(c.Args(), []any{1, 2}) {
		t.Errorf("unexpected args: %v", c.Args())
	}
}

func ExampleContext_Store() {
	ctx := slogctx.NewContext("one", 1).Store(context.Background())
	ctx = slogctx.NewContext("two", 2).Store(ctx)

	log := slog.New(slogctx.NewHandler(slog.NewTextHandler(os.Stdout,
		&slog.HandlerOptions{ReplaceAttr: slogutil.RemoveTime})))

	log.InfoContext(ctx, "Hello")
	// Output:
	// level=INFO msg=Hello two=2
}
