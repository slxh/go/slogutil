// Package slogctx contains utilities for storing log information in context.
// This can be used to log request information that is passed along with the context for the request.
// See the examples below.
package slogctx
