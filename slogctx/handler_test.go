package slogctx_test

import (
	"context"
	"io"
	"log/slog"
	"os"
	"testing"

	"gitlab.com/slxh/go/slogutil"
	"gitlab.com/slxh/go/slogutil/slogctx"
)

func ExampleDefault() {
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		ReplaceAttr: slogutil.RemoveTime,
	})))

	handler := slogctx.Default().
		WithAttrs([]slog.Attr{slog.Int("one", 1)})

	slog.New(handler).InfoContext(context.Background(), "Hello")
	// Output:
	// level=INFO msg=Hello one=1
}

func newLogger(w io.Writer) *slog.Logger {
	return slog.New(slogctx.NewHandler(slog.NewTextHandler(w, &slog.HandlerOptions{
		ReplaceAttr: slogutil.RemoveTime,
		Level:       slog.LevelDebug,
	})))
}

func TestHandler_Handle_allocs(t *testing.T) {
	logger := newLogger(io.Discard)
	ctx := slogctx.With(context.Background(), "one", 1, "two", 2, "three", 3)

	allocs := testing.AllocsPerRun(10000, func() {
		logger.DebugContext(ctx, "Test", "four", 4, "five", 5)
	})

	if allocs > 2 {
		t.Errorf("allocs = %v; want <= 2", allocs)
	}
}

func BenchmarkHandler_Handle(b *testing.B) {
	textHandler := slog.NewTextHandler(io.Discard, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	})
	ctx := slogctx.With(context.Background())

	benchmarks := map[string]struct {
		Logger *slog.Logger
		Ctx    context.Context //nolint:containedctx // used in test
	}{
		"text background": {
			Logger: slog.New(textHandler),
			Ctx:    context.Background(),
		},
		"slogctx background": {
			Logger: slog.New(slogctx.NewHandler(textHandler)),
			Ctx:    context.Background(),
		},
		"text ctx": {
			Logger: slog.New(textHandler),
			Ctx:    ctx,
		},
		"slogctx ctx": {
			Logger: slog.New(slogctx.NewHandler(textHandler)),
			Ctx:    ctx,
		},
	}

	for name, bm := range benchmarks {
		b.Run(name, func(b *testing.B) {
			b.ReportAllocs()

			for i := 0; i < b.N; i++ {
				bm.Logger.Info("Test", "one", 1)
			}
		})
	}
}

func ExampleHandler_WithAttrs() {
	textHandler := slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		ReplaceAttr: slogutil.RemoveTime,
	})

	handler := slogctx.NewHandler(textHandler).
		WithAttrs([]slog.Attr{slog.Int("one", 1)})

	ctx := slogctx.With(context.Background(), "two", 2)

	slog.New(handler).InfoContext(ctx, "Hello")
	// Output:
	// level=INFO msg=Hello one=1 two=2
}

func ExampleHandler_WithGroup() {
	textHandler := slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		ReplaceAttr: slogutil.RemoveTime,
	})

	handler := slogctx.NewHandler(textHandler).
		WithGroup("group")

	ctx := slogctx.With(context.Background(), "one", 1)

	slog.New(handler).InfoContext(ctx, "Hello", "two", 2)
	// Output:
	// level=INFO msg=Hello group.two=2 group.one=1
}
