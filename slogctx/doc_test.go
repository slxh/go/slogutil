package slogctx_test

import (
	"context"
	"log/slog"
	"net/http"
	"os"

	"gitlab.com/slxh/go/slogutil"
	"gitlab.com/slxh/go/slogutil/slogctx"
)

// Pass HTTP request information on with the context.
func Example_http() {
	log := slog.New(slogctx.NewHandler(
		slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{ReplaceAttr: slogutil.RemoveTime})))

	handler := func(_ http.ResponseWriter, r *http.Request) {
		log.InfoContext(r.Context(), "Got request")
	}

	http.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := slogctx.With(r.Context(), "request_id", "1234", "method", r.Method, "path", r.URL.Path)

		handler(w, r.WithContext(ctx))
	}))

	// Perform a test request
	req, _ := http.NewRequestWithContext(context.Background(), http.MethodGet, "/something", nil)
	http.DefaultServeMux.ServeHTTP(nil, req)

	// Output:
	// level=INFO msg="Got request" request_id=1234 method=GET path=/something
}
