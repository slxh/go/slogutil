// Package slogutil implements simple utilities for working with [slog].
package slogutil

import "log/slog"

// RemoveTime implements a ReplaceAttr function for [slog.HandlerOptions] that removes time timestamp.
func RemoveTime(groups []string, a slog.Attr) slog.Attr {
	if a.Key == slog.TimeKey && len(groups) == 0 {
		return slog.Attr{}
	}

	return a
}

// FormatTime returns a ReplaceAttr function for [slog.HandlerOptions] that
// formats the time using the given [time] format.
// See https://pkg.go.dev/time#pkg-constants for details.
func FormatTime(layout string) func(groups []string, a slog.Attr) slog.Attr {
	return func(groups []string, a slog.Attr) slog.Attr {
		if a.Key == slog.TimeKey && len(groups) == 0 {
			return slog.String(slog.TimeKey, a.Value.Time().Format(layout))
		}

		return a
	}
}
