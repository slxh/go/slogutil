package slogutil

import (
	"errors"
	"log/slog"
)

type slogError struct {
	attr slog.Attr
	err  error
	msg  string
}

// Error wraps an error with the given attributes for log context.
// The error expands to a group of key-value pairs when logged with [slog] (similar to [slog.Group]),
// with the `msg` key set to the error message.
// The error can be wrapped without losing attributes.
func Error(err error, args ...any) error {
	return ErrorMsg("", err, args...)
}

// ErrorMsg wraps an error and an additional message that is prefixed to the error message.
// This allows for additional context when the error could be used in other places than with [slog].
// It is otherwise identical to [Error].
func ErrorMsg(msg string, err error, args ...any) error {
	e := new(slogError)

	if errors.As(err, e) || errors.As(err, &e) {
		args = append(args, e.attr)
	}

	return &slogError{
		msg:  msg,
		err:  err,
		attr: slog.Group("", args...),
	}
}

// Error returns the error message as a string.
// It implements the [error] interface.
func (s slogError) Error() string {
	if s.msg != "" {
		return s.msg + ": " + s.err.Error()
	}

	return s.err.Error()
}

// Unwrap returns the errors wrapped by this error.
func (s slogError) Unwrap() error {
	return s.err
}

// LogValue returns the log [slog.Value] for this error.
func (s slogError) LogValue() slog.Value {
	return slog.GroupValue(slog.String("msg", s.err.Error()), s.attr)
}

// ErrorAttr returns a [slog.Attr] for an error.
// The attributes from an error produced by [Error] or [ErrorMsg] are included.
func ErrorAttr(key string, err error) slog.Attr {
	e := new(slogError)

	if errors.As(err, e) || errors.As(err, &e) {
		return slog.Group(key, slog.String("msg", err.Error()), e.attr)
	}

	return slog.String(key, err.Error())
}
