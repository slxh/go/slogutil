package slogutil_test

import (
	"flag"
	"fmt"
	"io"
	"log/slog"
	"os"
	"testing"

	"gitlab.com/slxh/go/slogutil"
)

func ExampleParseLevel() {
	level, _ := slogutil.ParseLevel("debug")

	fmt.Println(level)
	// Output:
	// DEBUG
}

func TestParseLevel(t *testing.T) {
	_, err := slogutil.ParseLevel("invalid")
	exp := `parse log level: slog: level string "invalid": unknown name`

	if err.Error() != exp {
		t.Errorf("got:\n  %q\nwant:\n  %q", err, exp)
	}
}

func ExampleMustParseLevel() {
	fmt.Println(slogutil.MustParseLevel("debug"))
	// Output:
	// DEBUG
}

func TestMustParseLevel(t *testing.T) {
	panicked := false

	func() {
		defer func() {
			panicked = recover() != nil
		}()

		slogutil.MustParseLevel("invalid")
	}()

	if !panicked {
		t.Error("expected panic")
	}
}

func ExampleFlag() {
	var level slog.LevelVar

	set := flag.NewFlagSet("test", flag.ContinueOnError)
	set.SetOutput(os.Stdout)
	set.Var(slogutil.Flag(&level), "log-level", "log level")

	fmt.Println("help:")
	set.PrintDefaults()

	set.Parse([]string{"-log-level", "debug"})
	fmt.Printf("parsed value:\n  %s", level.Level())
	// Output:
	// help:
	//   -log-level value
	//     	log level (default INFO)
	// parsed value:
	//   DEBUG
}

func TestFlag(t *testing.T) {
	var level slog.LevelVar

	set := flag.NewFlagSet("test", flag.ContinueOnError)
	set.SetOutput(io.Discard)
	set.Var(slogutil.Flag(&level), "log-level", "log level")

	err := set.Parse([]string{"-log-level", "invalid"})
	exp := `invalid value "invalid" for flag -log-level: set log level: ` +
		`slog: level string "invalid": unknown name`

	if err.Error() != exp {
		t.Errorf("got:\n  %q\nwant:\n  %q", err, exp)
	}
}
