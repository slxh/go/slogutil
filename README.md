# slogutil

`slogutil` is a small collection of zero-dependency packages with utilities for working with the [`log/slog`][slog] package.

**Note**: `slogutil` requires Go 1.21 or higher.

## Usage

Usage is described in the package documentation:

- [`slogutil`][slogutil] contains general utilties for working with log/slog, including error logging.
- [`slogutil/slogctx`][slogctx] contains functionality to store log information in context.
- [`slogutil/syslog`][syslog] contains a `slog.Handler` that logs to syslog.

[slog]: https://pkg.go.dev/log/slog
[slogutil]: https://pkg.go.dev/gitlab.com/slxh/go/slogutil
[slogctx]: https://pkg.go.dev/gitlab.com/slxh/go/slogutil/slogctx
[syslog]: https://pkg.go.dev/gitlab.com/slxh/go/slogutil/syslog
