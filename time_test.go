package slogutil_test

import (
	"context"
	"log/slog"
	"os"
	"time"

	"gitlab.com/slxh/go/slogutil"
)

func ExampleRemoveTime() {
	logger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		ReplaceAttr: slogutil.RemoveTime,
	}))

	logger.Info("Hello world")

	// Output:
	// level=INFO msg="Hello world"
}

func ExampleFormatTime() {
	logger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		ReplaceAttr: slogutil.FormatTime(time.ANSIC),
	}))

	logger.Handler().Handle(context.Background(), slog.NewRecord(
		time.Date(2021, time.July, 5, 8, 23, 19, 0, time.UTC),
		slog.LevelInfo, "Hello world", 0))

	// Output:
	// time="Mon Jul  5 08:23:19 2021" level=INFO msg="Hello world"
}
