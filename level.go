package slogutil

import (
	"flag"
	"fmt"
	"log/slog"
)

// ParseLevel parses the given log level and returns the resulting [slog.Level].
func ParseLevel(level string) (slog.Level, error) {
	var parsed slog.Level

	if err := parsed.UnmarshalText([]byte(level)); err != nil {
		return slog.LevelInfo, fmt.Errorf("parse log level: %w", err)
	}

	return parsed, nil
}

// MustParseLevel parses the given log level and returns the resulting [slog.Level].
// It panics when the given level is not valid.
func MustParseLevel(level string) slog.Level {
	parsed, err := ParseLevel(level)
	if err != nil {
		panic(err)
	}

	return parsed
}

type flagValue struct {
	v *slog.LevelVar
}

// Flag returns a [flag.Value] for a [slog.LevelVar].
func Flag(v *slog.LevelVar) flag.Value {
	return flagValue{v: v}
}

func (f flagValue) String() string {
	if f.v == nil {
		return "invalid"
	}

	return f.v.Level().String()
}

func (f flagValue) Set(s string) error {
	if err := f.v.UnmarshalText([]byte(s)); err != nil {
		return fmt.Errorf("set log level: %w", err)
	}

	return nil
}
